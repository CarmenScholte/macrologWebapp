import {Portion} from './portion';
import {Food} from './food';

export class FoodSearchable {


constructor(public food: Food, public portion: Portion) {

	}
} 
