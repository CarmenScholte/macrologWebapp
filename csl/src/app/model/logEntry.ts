export class LogEntry {

  public id: number;
  public meal: string;
	public food;
	public portion;
	public multiplier = 1;
  public day;
  public macrosCalculated;

	constructor () {
	}
}
